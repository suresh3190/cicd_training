
// Task 2 : Variable Hoisting

// let1=100    / Hoisting won work for let - cannot be accessed before initialization
// console.log(let1)

var1="Naresh"
console.log(var1)

// const1=2000   / For constant initialization and declaration should happened at the same time, 
// so for Hoisting there is no possibility (Hoisting happens only with declarations)
// console.log(const1)

let let1
var var1 
const const1=5000

// Task 3 : Function Hoisting

test()  // Only declarations can be Hoisted
// dup  // Cant be hoisted as its not a declaration kind of expression
console.log(exp)     // Prints undefined as its expression and Expressions & Initilizations cant be hoisted

function test(){    // Function Declaration
    var i="Function Declaration"
    console.log(i)
}

dup=test()
dup

var exp=function (){    // Function Expression
    var abc="Function Ecpression"
    console.log(abc)
}

console.log(exp)