
var emp = {name:"Suresh", age:25, sal:5000};
function JavaScriptObject(){

    document.write("Employee Name : "+emp.name+"<br>");
    document.write("Employee Age : "+emp.age+"<br>");
    document.write("Employee Salary : "+emp.sal);
}

var nameRegister = [1, "Suresh", "Ramesh", "Hareesh"];
function JavaScriptArray(){

    document.write("Employee 1 : "+nameRegister[0]+"<br>");
    document.write("Employee 2 : "+nameRegister[1]+"<br>");
    document.write("Employee 3 : "+nameRegister[2]+"<br>");
    document.write("Employee 4 : "+nameRegister[3]);
}

function ForInLoopObject(){
    var i=1;
    for (var x in emp) {
        // document.write(i+" Property : "+ emp[x]+"<br>");
        console.log(i+" Property : "+ emp[x]);
        i==i++;
      }
}

function ForOfLoopArray(){
    var i=1;
    for (var x of nameRegister) {
        // document.write(i+" Array Value : "+ x+"<br>");
        console.log(i+" Array Value : "+ x);
        i==i++;
      }
}